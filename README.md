# ld48 - Stabalot




This is my Ludum Dare #48 gamejam compo submission.

The theme this year was "Deeper and deeper"; so my interpretation of the theme was a fencer who attacks monsters and has to penetrate deeper and deeper into the armor to get a successful hit. The player must manipulate the armor with his rapier to get a good hit.

All assets created by me. The game was built in about 36 hours in pico-8

A more detailed Readme and post mortem to come...
